﻿using System;

namespace Command
{
    class Program
    {
        static void Main(string[] args)
        {
            //RemoteControl remoteControl = new RemoteControl();
            //Light light = new Light();
            //GarageDoor door = new GarageDoor();
            //Stereo stereo = new Stereo();

            //LightOnCommand lightOn = new LightOnCommand(light);
            //LightOffCommand lightOff = new LightOffCommand(light);
            //StereoOnWithCDCommand stereoOnWithCD = new StereoOnWithCDCommand(stereo);
            //GarageDoorOpenCommand garageOpen = new GarageDoorOpenCommand(door);

            //remoteControl.SetCommand(0, lightOn, lightOff);
            //remoteControl.SetCommand(1, garageOpen, garageOpen);
            //remoteControl.SetCommand(2, stereoOnWithCD, stereoOnWithCD);

            //Console.WriteLine(remoteControl.ToString());

            //remoteControl.OnButtonWasPushed(0);
            //remoteControl.OffButtonWasPushed(0);
            //remoteControl.OnButtonWasPushed(1);
            //remoteControl.OnButtonWasPushed(2);
            //remoteControl.OnButtonWasPushed(3);

            //RemoteControl remoteControl = new RemoteControl();

            Light light = new Light();

            LightOnCommand lightOn = new LightOnCommand(light);
            LightOffCommand lightOff = new LightOffCommand(light);

            //remoteControl.SetCommand(0, lightOn, lightOff);

            //remoteControl.OnButtonWasPushed(0);
            //remoteControl.OffButtonWasPushed(0);

            //Console.WriteLine(remoteControl.ToString());

            //remoteControl.UndoButtonWasPushed(0);
            //remoteControl.OffButtonWasPushed(0);
            //remoteControl.OnButtonWasPushed(0);

            RemoteControl remoteControl = new RemoteControl();

            CeilingFan ceilingFan = new CeilingFan("Living Room");

            CeilingFanOffCommand ceilingFanOff = new CeilingFanOffCommand(ceilingFan);
            CeilingFanLowCommand ceilingFanLow = new CeilingFanLowCommand(ceilingFan);
            CeilingFanMediumCommand ceilingFanMedium = new CeilingFanMediumCommand(ceilingFan);
            CeilingFanHighCommand ceilingFanHigh = new CeilingFanHighCommand(ceilingFan);

            //remoteControl.SetCommand(0, ceilingFanMedium, ceilingFanOff);
            //remoteControl.SetCommand(1, ceilingFanHigh, ceilingFanOff);
            //remoteControl.SetCommand(2, ceilingFanLow, ceilingFanOff);

            //remoteControl.OnButtonWasPushed(0);
            //remoteControl.OffButtonWasPushed(0);
            //remoteControl.UndoButtonWasPushed(0);

            //remoteControl.OnButtonWasPushed(1);
            //remoteControl.UndoButtonWasPushed(1);

            ICommand[] partyOn = { ceilingFanHigh, lightOn , ceilingFanMedium };
            ICommand[] partyOff = { lightOff, ceilingFanOff };

            MacroCommand partyOnMacro = new MacroCommand(partyOn);
            MacroCommand partyOffMacro = new MacroCommand(partyOff);

            remoteControl.SetCommand(0, partyOnMacro, partyOffMacro);

            Console.WriteLine(remoteControl);

            remoteControl.OnButtonWasPushed(0);
            remoteControl.OffButtonWasPushed(0);
            remoteControl.UndoButtonWasPushed(0);

            Console.ReadKey();
        }
    }
}
