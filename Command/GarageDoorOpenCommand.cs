﻿using System;

namespace Command
{
    public class GarageDoorOpenCommand : ICommand
    {
        GarageDoor door;

        public GarageDoorOpenCommand(GarageDoor door)
        {
            this.door = door;
        }

        public void Execute()
        {
            door.Up();
        }

        public void Undo()
        {
            door.Down();
        }
    }
}
