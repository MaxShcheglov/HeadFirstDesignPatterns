﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command
{
    public enum CeilingFanSpeed
    {
        Off,
        Low,
        Medium,
        High
    }

    public class CeilingFan
    {
        private CeilingFanSpeed _speed;

        public string Location { get; set; }

        public CeilingFanSpeed Speed
        {
            get
            {
                return _speed;
            }
            set
            {
                _speed = value;
                Console.WriteLine($"Speed changed to {_speed}");
            }
        }

        public CeilingFan(string location)
        {
            Location = location;
            Speed = CeilingFanSpeed.Off;
        }

        public void High()
        {
            Speed = CeilingFanSpeed.High;
        }

        public void Medium()
        {
            Speed = CeilingFanSpeed.Medium;
        }

        public void Low()
        {
            Speed = CeilingFanSpeed.Low;
        }

        public void Off()
        {
            Speed = CeilingFanSpeed.Off;
        }
    }
}
