﻿using System;

namespace Command
{
    public class GarageDoor
    {
        public void Up() => Console.WriteLine("Garage Door is Open");
        public void Down() => Console.WriteLine("Garage Door is Close");
        public void Stop() { }
        public void LightOn() { }
        public void LightOff() { }
    }
}
