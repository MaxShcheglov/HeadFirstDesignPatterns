﻿using System.Text;

namespace Command
{
    public class RemoteControl
    {
        ICommand[] onCommands;
        ICommand[] offCommands;
        ICommand UndoCommand;

        public RemoteControl()
        {
            onCommands = new ICommand[7];
            offCommands = new ICommand[7];

            ICommand noCommand = new NoCommand();
            for (int i = 0; i < 7; i++)
            {
                onCommands[i] = noCommand;
                offCommands[i] = noCommand;
            }
            UndoCommand = noCommand;
        }

        public void SetCommand(int slot, ICommand onCommand, ICommand offCommand)
        {
            onCommands[slot] = onCommand;
            offCommands[slot] = offCommand;
        }

        public void OnButtonWasPushed(int slot)
        {
            onCommands[slot]?.Execute();
            UndoCommand = onCommands[slot];
        }

        public void OffButtonWasPushed(int slot)
        {
            offCommands[slot]?.Execute();
            UndoCommand = offCommands[slot];
        }

        public void UndoButtonWasPushed(int slot)
        {
            UndoCommand.Undo();
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("\n--- remote control ---\n");
            for(int i = 0; i < onCommands.Length; i++)
            {
                builder.AppendLine($"[slot {i}] {onCommands[i].GetType().Name}   {offCommands[i].GetType().Name}");
            }
            return builder.ToString();
        }
    }
}
