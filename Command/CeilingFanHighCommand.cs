﻿namespace Command
{
    public class CeilingFanHighCommand : ICommand
    {
        CeilingFan ceilingFan;
        CeilingFanSpeed prevSpeed;

        public CeilingFanHighCommand(CeilingFan ceilingFan)
        {
            this.ceilingFan = ceilingFan;
        }

        public void Execute()
        {
            prevSpeed = ceilingFan.Speed;
            ceilingFan.High();
        }

        public void Undo()
        {
            switch (prevSpeed)
            {
                case CeilingFanSpeed.Off:
                    ceilingFan.Off();
                    break;
                case CeilingFanSpeed.Low:
                    ceilingFan.Low();
                    break;
                case CeilingFanSpeed.Medium:
                    ceilingFan.Medium();
                    break;
                case CeilingFanSpeed.High:
                    ceilingFan.High();
                    break;
            }
        }
    }
}
