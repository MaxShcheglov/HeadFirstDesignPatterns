﻿namespace Command
{
    public class CeilingFanOffCommand : ICommand
    {
        CeilingFan ceilingFan;
        CeilingFanSpeed prevSpeed;

        public CeilingFanOffCommand(CeilingFan ceilingFan)
        {
            this.ceilingFan = ceilingFan;
        }

        public void Execute()
        {
            prevSpeed = ceilingFan.Speed;
            ceilingFan.Off();
        }

        public void Undo()
        {
            switch (prevSpeed)
            {
                case CeilingFanSpeed.Off:
                    ceilingFan.Off();
                    break;
                case CeilingFanSpeed.Low:
                    ceilingFan.Low();
                    break;
                case CeilingFanSpeed.Medium:
                    ceilingFan.Medium();
                    break;
                case CeilingFanSpeed.High:
                    ceilingFan.High();
                    break;
            }
        }
    }
}
