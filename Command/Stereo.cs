﻿namespace Command
{
    public class Stereo
    {
        public void On() { }
        public void Off() { }
        public void SetCD() { }
        public void SetDVD() { }
        public void SetRadio() { }
        public void SetVolume(int value) { }
    }
}
