﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge
{
    class Program
    {
        static void Main(string[] args)
        {
            //Abstraction ab = new RefinedAbstraction();

            //ab.Implementor = new ConcreteImplementorA();
            //ab.Operation();

            //ab.Implementor = new ConcreteImplementorB();
            //ab.Operation();

            RemoteControl remoteControl = new RemoteControl();

            remoteControl.TV = new RCA();
            remoteControl.On();

            remoteControl.TV = new Sony();
            remoteControl.Off();

            Console.ReadKey();
        }
    }

    public class Abstraction
    {
        protected Implementor implementor;

        public Implementor Implementor
        {
            set { implementor = value; }
        }

        public virtual void Operation()
        {
            implementor.Operation();
        }
    }

    public abstract class Implementor
    {
        public abstract void Operation();
    }

    public class RefinedAbstraction : Abstraction
    {
        public override void Operation()
        {
            implementor.Operation();
        }
    }

    public class ConcreteImplementorA : Implementor
    {
        public override void Operation()
        {
            Console.WriteLine("ConcreteImplementorA Operation");
        }
    }

    public class ConcreteImplementorB : Implementor
    {
        public override void Operation()
        {
            Console.WriteLine("ConcreteImplementorB Operation");
        }
    }


    public class RemoteControl
    {
        protected TV tv;

        public TV TV
        {
            set { tv = value; }
        }

        public virtual void On()
        {
            tv.On();
        }

        public virtual void Off()
        {
            tv.Off();
        }

        public virtual void SetChannel(Channel channel)
        {
            tv.TuneChannel(channel);
        }
    }

    public class ConcreteRemote : RemoteControl
    {
        protected Channel currentStation;

        public void NextChannel()
        {
            //SetChannel(currentStation + 1);
        }

        public void PreviousChannel()
        {
            //SetChannel(currentStation - 1);
        }
    }

    public class Channel { }

    public abstract class TV
    {
        public abstract void On();
        public abstract void Off();
        public abstract void TuneChannel(Channel channel);
    }

    public class RCA : TV
    {
        public override void Off()
        {
            Console.WriteLine(System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." +
                              System.Reflection.MethodBase.GetCurrentMethod().Name);
        }

        public override void On()
        {
            Console.WriteLine(System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." +
                              System.Reflection.MethodBase.GetCurrentMethod().Name);
        }

        public override void TuneChannel(Channel channel)
        {
            Console.WriteLine(System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." +
                              System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }

    public class Sony : TV
    {
        public override void Off()
        {
            Console.WriteLine(System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." +
                              System.Reflection.MethodBase.GetCurrentMethod().Name);
        }

        public override void On()
        {
            Console.WriteLine(System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." +
                              System.Reflection.MethodBase.GetCurrentMethod().Name);
        }

        public override void TuneChannel(Channel channel)
        {
            Console.WriteLine(System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." +
                              System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }

}
