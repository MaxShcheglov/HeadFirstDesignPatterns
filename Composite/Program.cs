﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite
{
    class Program
    {
        static void Main(string[] args)
        {
            MenuComponent pancakeHouseMenu = new Menu("PANCAKE HOUSE MENU", "Breakfast");
            MenuComponent dinerMenu = new Menu("DINER MENU", "Lunch");
            MenuComponent cafeMenu = new Menu("CAFE MENU", "Dinner");
            MenuComponent dessertMenu = new Menu("DESSERT MENU", "Dessert Of Course");

            Menu allMenus = new Menu("ALL MENUS", "ALL MENUS COMBINED");

            allMenus.Add(pancakeHouseMenu);
            allMenus.Add(dinerMenu);
            allMenus.Add(cafeMenu);

            dinerMenu.Add(new MenuItem("pasta", "spaggetti", true, 3.89));

            dinerMenu.Add(dessertMenu);

            dessertMenu.Add(new MenuItem("Apple Pie", "apple pie", true, 1.59));

            Waitress waitress = new Waitress(allMenus);

            //waitress.PrintMenu();
            waitress.PrintVegetarianMenu();

            //Composite root = new Composite("root");
            //root.Add(new Leaf("Leaf A"));
            //root.Add(new Leaf("Leaf B"));

            //Composite comp = new Composite("Composite X");
            //comp.Add(new Leaf("Leaf XA"));
            //comp.Add(new Leaf("Leaf XB"));

            //root.Add(comp);
            //root.Add(new Leaf("Leaf C"));

            //// Add and remove a leaf
            //Leaf leaf = new Leaf("Leaf D");
            //root.Add(leaf);
            //root.Remove(leaf);

            //// Recursively display tree
            //root.Display(1);

            Console.ReadKey();
        }
    }

    //public class CompositeIterator : IEnumerator
    //{
    //    Stack stack = new Stack();

    //    public CompositeIterator(IEnumerator iterator)
    //    {
    //        stack.Push(iterator);
    //    }

    //    public object Current
    //    {
    //        get
    //        {
    //            if (MoveNext())
    //            {
    //                IEnumerator iterator = (IEnumerator)stack.Peek();
    //                IMenuComponent component = (IMenuComponent)iterator.Current;                    
    //                if (component != null && component is Menu) stack.Push(component.CreateIterator());
    //                return component;
    //            }
    //            else
    //            {
    //                return null;
    //            }
    //        }
    //    }

    //    public bool MoveNext()
    //    {
    //        if (stack.Count  == 0)
    //        {
    //            return false;
    //        }
    //        else
    //        {
    //            IEnumerator iterator = (IEnumerator)stack.Peek();
    //            if (!iterator.MoveNext())
    //            {
    //                stack.Pop();
    //                return iterator.MoveNext();
    //            }
    //            else
    //            {
    //                return true;
    //            }
    //        }
    //    }

    //    public void Reset()
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    public class NullIterator : IEnumerator
    {
        public object Current => null;

        public bool MoveNext()
        {
            return false;
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }
    }

    public interface IMenuComponent
    {
        string Description { get; }
        bool IsVegetarian { get; }
        string Name { get; }
        double Price { get; }

        void Add(IMenuComponent menuComponent);
        IMenuComponent GetChild(int i);
        void Print();
        void Remove(IMenuComponent menuComponent);
        IEnumerator CreateIterator();
    }

    public abstract class MenuComponent : IMenuComponent
    {
        protected string _name;
        protected string _description;
        protected bool _isVegetable;
        protected double _price;

        public abstract void Add(IMenuComponent menuComponent);
        public abstract void Remove(IMenuComponent menuComponent);
        public abstract IMenuComponent GetChild(int i);

        public abstract string Name { get; set; }
        public abstract string Description { get; set; }
        public abstract double Price { get; set; }
        public abstract bool IsVegetarian { get; set; }

        public abstract void Print();

        public abstract IEnumerator CreateIterator();
    }

    public class MenuItem : MenuComponent
    {
        public override string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public override string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public override bool IsVegetarian
        {
            get { return _isVegetable; }
            set { _isVegetable = value; }
        }

        public override double Price
        {
            get { return _price; }
            set { _price = value; }
        }

        public MenuItem(string name, string description, bool isVegetarian, double price)
        {
            Name = name;
            Description = description;
            IsVegetarian = isVegetarian;
            Price = price;
        }

        public override void Print()
        {
            var stringToPrint = $"{Name} ";
            if (IsVegetarian) stringToPrint += "(v) ,";
            stringToPrint += $"{Price} -- {Description}";

            Console.WriteLine(stringToPrint);
        }

        public override IEnumerator CreateIterator() => new NullIterator();

        public override void Add(IMenuComponent menuComponent)
        {
            throw new NotImplementedException();
        }

        public override void Remove(IMenuComponent menuComponent)
        {
            throw new NotImplementedException();
        }

        public override IMenuComponent GetChild(int i)
        {
            throw new NotImplementedException();
        }
    }

    public class Menu : MenuComponent
    {
        private List<IMenuComponent> menuComponents = new List<IMenuComponent>();
        private IEnumerator iterator = null;

        public Menu(string name, string description)
        {
            _name = name;
            _description = description;
        }

        public override void Add(IMenuComponent menuComponent)
        {
            menuComponents.Add(menuComponent);
        }

        public override void Remove(IMenuComponent menuComponent)
        {
            menuComponents.Remove(menuComponent);
        }

        public override IMenuComponent GetChild(int i)
        {
            return (IMenuComponent)menuComponents[i];
        }

        public override string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public override string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public override double Price { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public override bool IsVegetarian { get => true; set => throw new NotImplementedException(); }

        public override void Print()
        {
            Console.WriteLine($"{Name}, {Description}");

            IEnumerator iterator = menuComponents.GetEnumerator();
            while (iterator.MoveNext())
            {
                IMenuComponent menuComponent = (IMenuComponent)iterator.Current;
                menuComponent?.Print();
            }
        }

        public override IEnumerator CreateIterator()
        {
            if (iterator == null) iterator = menuComponents.GetEnumerator();
            return iterator;
        }
    }

    public class Waitress
    {
        IMenuComponent allMenus;

        public Waitress(IMenuComponent allMenus)
        {
            this.allMenus = allMenus;
        }

        public void PrintMenu()
        {
            allMenus.Print();
        }

        public void PrintVegetarianMenu()
        {
            IEnumerator iterator = allMenus.CreateIterator();
            Console.WriteLine("Vegetarian MENU");
            while (iterator.MoveNext())
            {
                MenuComponent menuComponent = (MenuComponent)iterator.Current;

                try
                {
                    if (menuComponent.IsVegetarian) menuComponent.Print();
                    //menuComponent.Print();
                }
                catch (Exception e) { }
            }
        }
    }
}
