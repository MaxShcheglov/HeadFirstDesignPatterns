﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{
    class Program
    {
        static void Main(string[] args)
        {
            PizzaStore nyPizzaStore = new NYPizzaStore();
            PizzaStore chicagoStore = new ChicagoPizzaStore();

            Pizza pizza = nyPizzaStore.OrderPizza("cheese");
            Console.WriteLine($"Ethan ordered a {pizza.Name}");

            pizza = chicagoStore.OrderPizza("cheese");
            Console.WriteLine($"Joel ordered a {pizza.Name}");

            Console.ReadKey();
        }
    }

    public abstract class PizzaStore
    {
        public Pizza OrderPizza(string type)
        {
            Pizza pizza = CreatePizza(type);

            pizza.Prepare();
            pizza.Bake();
            pizza.Cut();
            pizza.Box();

            return pizza;
        }

        protected abstract Pizza CreatePizza(string type); // this is a factory method
    }

    public class Dough { }
    public class Sauce { }
    public class Cheese { }
    public class Veggies { }
    public class Pepperoni { }
    public class Clams { }

    public abstract class Pizza
    {
        public string Name { get; set; }
        public Dough Dough { get; set; }
        public Sauce Sauce { get; set; }
        public Veggies[] Veggies { get; set; }
        public Cheese Cheese { get; set; }
        public Pepperoni Pepperoni { get; set; }
        public Clams Clam { get; set; }

        public Pizza()
        {
        }

        public abstract void Prepare();

        //public void Prepare()
        //{
        //    Console.WriteLine($"Preparing {Name}");
        //    Console.WriteLine($"Tossing dough");
        //    Console.WriteLine($"Adding sauce");
        //    Console.WriteLine($"Adding toppings: ");
        //    foreach (var topping in Toppings)
        //        Console.WriteLine($"\t{topping}");
        //}

        public void Bake()
        {
            Console.WriteLine("Bake for 25 minutes at 350");
        }

        public void Cut()
        {
            Console.WriteLine("Cutting the pizza into diagonal slices");
        }

        public void Box()
        {
            Console.WriteLine("Place pizza in official PizzaStore box");
        }
    }

    public class NYPizzaStore : PizzaStore
    {
        protected override Pizza CreatePizza(string type)
        {
            IPizzaIngredientFactory ingredientFactory = new NYPizzaIngredientFactory();

            switch (type)
            {
                case "cheese":
                    return new CheesePizza(ingredientFactory) { Name = "New York Style Cheese Pizza" };
                case "clam":
                    return new ClamPizza(ingredientFactory) { Name = "New York Style Clam Pizza" };
                default:
                    return null;
            }
        }
    }

    public class ChicagoPizzaStore : PizzaStore
    {
        protected override Pizza CreatePizza(string type)
        {
            IPizzaIngredientFactory ingredientFactory = new NYPizzaIngredientFactory();

            switch (type)
            {
                case "cheese":
                    return new CheesePizza(ingredientFactory) { Name = "Chicago Style Cheese Pizza" };
                case "clam":
                    return new ClamPizza(ingredientFactory) { Name = "Chicago Style Clam Pizza" };
                default:
                    return null;
            }
        }
    }

    public class CheesePizza : Pizza
    {
        private IPizzaIngredientFactory ingredientFactory;

        public CheesePizza(IPizzaIngredientFactory ingredientFactory)
        {
            this.ingredientFactory = ingredientFactory;
        }

        public override void Prepare()
        {
            Console.WriteLine($"Preparing {Name}");
            Dough = ingredientFactory.CreateDough();
            Sauce = ingredientFactory.CreateSauce();
            Cheese = ingredientFactory.CreateCheese();
        }
    }

    public class ClamPizza : Pizza
    {
        private IPizzaIngredientFactory ingredientFactory;

        public ClamPizza(IPizzaIngredientFactory ingredientFactory)
        {
            this.ingredientFactory = ingredientFactory;
        }

        public override void Prepare()
        {
            Console.WriteLine($"Preparing {Name}");
            Dough = ingredientFactory.CreateDough();
            Sauce = ingredientFactory.CreateSauce();
            Cheese = ingredientFactory.CreateCheese();
            Clam = ingredientFactory.CreateClam();
        }
    }

    public interface IPizzaIngredientFactory
    {
        Dough CreateDough();
        Sauce CreateSauce();
        Cheese CreateCheese();
        Veggies[] CreateVeggies();
        Clams CreateClam();
    }

    public class ThinCrustDogh : Dough { }
    public class MarinaraSauce : Sauce { }
    public class ReggianoCheese : Cheese { }
    public class Garlic : Veggies { }
    public class Onion : Veggies { }
    public class Mushroom : Veggies { }
    public class RedPepper : Veggies { }
    public class SlicedPepperoni : Pepperoni { }
    public class FreshClams : Clams { }

    public class NYPizzaIngredientFactory : IPizzaIngredientFactory
    {
        public Dough CreateDough()
        {
            return new ThinCrustDogh();
        }

        public Cheese CreateCheese()
        {
            return new ReggianoCheese();
        }

        public Clams CreateClam()
        {
            return new FreshClams();
        }

        public Sauce CreateSauce()
        {
            return new MarinaraSauce();
        }

        public Veggies[] CreateVeggies()
        {
            return new Veggies[] { new Garlic(), new Onion(), new Mushroom(), new RedPepper() };
        }
    }
}
