﻿using System;
using System.Collections.Generic;

namespace Builder
{
    class Program
    {
        static void Main(string[] args)
        {
            //Shop.VehicleBuilder builder;

            //Shop shop = new Shop();

            //builder = new Shop.ScooterBuilder();
            //shop.Construct(builder);
            //builder.Vehicle.Show();

            //builder = new Shop.CarBuilder();
            //shop.Construct(builder);
            //builder.Vehicle.Show();

            //builder = new Shop.MotorcycleBuilder();
            //shop.Construct(builder);
            //builder.Vehicle.Show();

            ISomeObjectBuilder builder = new SomeObjectBuilder().WithFirstComponent(new FirstComponent())
                                                                .WithSecondComponent(new SecondComponent())
                                                                .WIthThirdComponent(new ThirdComponent());

            SomeObject @object = builder.Build();            

            Console.ReadKey();
        }
    }

    public class Shop
    {
        public void Construct(VehicleBuilder vehicleBuilder)
        {
            vehicleBuilder.BuildFrame();
            vehicleBuilder.BuildEngine();
            vehicleBuilder.BuildWheels();
            vehicleBuilder.BuildDoors();
        }

        public abstract class VehicleBuilder
        {
            protected Vehicle vehicle;

            public Vehicle Vehicle { get { return vehicle; } }

            public abstract void BuildFrame();
            public abstract void BuildEngine();
            public abstract void BuildWheels();
            public abstract void BuildDoors();
        }

        public class MotorcycleBuilder : VehicleBuilder
        {
            public MotorcycleBuilder()
            {
                vehicle = new Vehicle("Motorcycle");
            }

            public override void BuildFrame()
            {
                vehicle["frame"] = "MotorCycle Frame";
            }

            public override void BuildEngine()
            {
                vehicle["engine"] = "500 cc";
            }

            public override void BuildWheels()
            {
                vehicle["wheels"] = "2";
            }

            public override void BuildDoors()
            {
                vehicle["doors"] = "0";
            }
        }

        public class CarBuilder : VehicleBuilder
        {
            public CarBuilder()
            {
                vehicle = new Vehicle("Car");
            }

            public override void BuildFrame()
            {
                vehicle["frame"] = "Car Frame";
            }

            public override void BuildEngine()
            {
                vehicle["engine"] = "2500 cc";
            }

            public override void BuildWheels()
            {
                vehicle["wheels"] = "4";
            }

            public override void BuildDoors()
            {
                vehicle["doors"] = "4";
            }
        }

        public class ScooterBuilder : VehicleBuilder
        {
            public ScooterBuilder()
            {
                vehicle = new Vehicle("Scooter");
            }

            public override void BuildFrame()
            {
                vehicle["frame"] = "Scooter Frame";
            }

            public override void BuildEngine()
            {
                vehicle["engine"] = "50 cc";
            }

            public override void BuildWheels()
            {
                vehicle["wheels"] = "2";
            }

            public override void BuildDoors()
            {
                vehicle["doors"] = "0";
            }
        }

        public class Vehicle
        {
            private string _vehicleType;
            private Dictionary<string, string> _parts = new Dictionary<string, string>();

            public Vehicle(string vehicleType)
            {
                this._vehicleType = vehicleType;
            }

            public string this[string key]
            {
                get { return _parts[key]; }
                set { _parts[key] = value; }
            }

            public void Show()
            {
                Console.WriteLine("\n---------------------------");
                Console.WriteLine("Vehicle Type: {0}", _vehicleType);
                Console.WriteLine(" Frame : {0}", _parts["frame"]);
                Console.WriteLine(" Engine : {0}", _parts["engine"]);
                Console.WriteLine(" #Wheels: {0}", _parts["wheels"]);
                Console.WriteLine(" #Doors : {0}", _parts["doors"]);
            }
        }
    }

    public interface ISomeObjectBuilder
    {
        ISomeObjectBuilder WithFirstComponent(FirstComponent firstComponent);
        ISomeObjectBuilder WithSecondComponent(SecondComponent secondComponent);
        ISomeObjectBuilder WIthThirdComponent(ThirdComponent thirdComponent);
        SomeObject Build();
    }

    public class SomeObjectBuilder : ISomeObjectBuilder
    {
        protected FirstComponent _firstComponent;
        protected SecondComponent _secondComponent;
        protected ThirdComponent _thirdComponent;

        public SomeObject Build()
        {
            return new SomeObject(_firstComponent, _secondComponent, _thirdComponent);
        }

        public ISomeObjectBuilder WithFirstComponent(FirstComponent firstComponent)
        {
            _firstComponent = firstComponent;

            return this;
        }

        public ISomeObjectBuilder WithSecondComponent(SecondComponent secondComponent)
        {
            _secondComponent = secondComponent;

            return this;
        }

        public ISomeObjectBuilder WIthThirdComponent(ThirdComponent thirdComponent)
        {
            _thirdComponent = thirdComponent;

            return this;
        }
    }

    public class SomeObject
    {
        protected FirstComponent _firstComponent;
        protected SecondComponent _secondComponent;
        protected ThirdComponent _thirdComponent;

        public SomeObject(FirstComponent firstComponent, 
                          SecondComponent secondComponent,
                          ThirdComponent thirdComponent)
        {
            _firstComponent = firstComponent;
            _secondComponent = secondComponent;
            _thirdComponent = thirdComponent;
        }
    }

    public class FirstComponent { }
    public class SecondComponent { }
    public class ThirdComponent { }
}
