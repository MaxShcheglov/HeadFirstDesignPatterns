﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using State;
using System.Reflection;

namespace Proxy
{
    class Program
    {
        static void Main(string[] args)
        {
            GumballMonitorTesting();

            Console.ReadKey();
        }

        static void MatchMakingTesting()
        {

        }

        private static object GetOwnerProxy(IPersonBean person)
        {
            return (IPersonBean)Proxy.NewProxyInstance(
                null,
                null,
                new OwnerInvocationHandler(person));
        }

        private static object GetNonOwnerProxy(IPersonBean person)
        {
            return (IPersonBean)Proxy.NewProxyInstance(
                null,
                null,
                new NonOwnerInvocationHandler(person));
        }

        static void GumballMonitorTesting()
        {
            string[] location = { "rmi://first.path.com",
                                  "rmi://second.path.com",
                                  "rmi://third.path.com" };

            GumballMonitor[] monitor = new GumballMonitor[location.Length];

            for (int i = 0; i < location.Length; i++)
            {
                try
                {
                    //IGumballMachineRemote machine = (IGumballMachineRemote)Naming.Lookup(location[i]);
                    //monitor[i] = new GumballMonitor(machine);
                }
                catch (Exception e)
                {
                    // print stacktrace
                }
            }

            for (int i = 0; i < monitor.Length; i++)
            {
                monitor[i].Report();
            }
        }
    }

    public static class Proxy
    {
        public static object NewProxyInstance(object loader, object interfaces, object invokationHandler)
        {
            return null;
        }
    }

    public interface IPersonBean
    {
        string Name { get; set; }
        string Gender { get; set; }
        string Interests { get; set; }
        int HotOrNotrating { get; set; }
    }

    public class PersonBeanImpl : IPersonBean
    {
        private string name;
        private string gender;
        private string interests;
        private int rating;
        private int ratingCount;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Gender
        {
            get { return gender; }
            set { gender = value; }
        }

        public string Interests
        {
            get { return interests; }
            set { interests = value; }
        }

        public int HotOrNotrating
        {
            get
            {
                if (ratingCount == 0) return 0;
                return (rating / ratingCount);
            }
            set
            {
                rating += value;
                ratingCount++;
            }
        }
    }

    public interface IInvocationHandler
    {
        object Invoke(object proxy, MethodInfo method, object[] args);
    }

    public class OwnerInvocationHandler : IInvocationHandler
    {
        private IPersonBean person;

        public OwnerInvocationHandler(IPersonBean person)
        {
            this.person = person;
        }

        public object Invoke(object proxy, MethodInfo method, object[] args)
        {
            try
            {
                if (method.Name.Equals("HotOrNotRating"))
                    throw new Exception();

                method.Invoke(person, args);
            }
            catch (Exception) { }
            return null;
        }
    }

    public class NonOwnerInvocationHandler : IInvocationHandler
    {
        private IPersonBean person;

        public NonOwnerInvocationHandler(IPersonBean persons)
        {
            this.person = person;
        }

        public object Invoke(object proxy, MethodInfo method, object[] args)
        {
            try
            {
                if (!method.Name.Equals("HotOrNotRating"))
                    throw new Exception();

                method.Invoke(person, args);
            }
            catch (Exception) { }
            return null;
        }
    }
}
