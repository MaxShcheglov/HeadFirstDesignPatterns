﻿using System;
using State;

namespace Proxy
{
    public interface IGumballMachineRemote
    {
        int Count { get; }
        string Location { get; }
        IState State { get; }
    }

    public class GumballMonitor
    {
        IGumballMachineRemote machine;

        public GumballMonitor(IGumballMachineRemote machine)
        {
            this.machine = machine;
        }

        public void Report()
        {
            Console.WriteLine($"Gumball Machine: {machine.Location}");
            Console.WriteLine($"Current inventory: {machine.Count} gumballs");
            Console.WriteLine($"Current state: {machine.State}");
        }
    }


}
