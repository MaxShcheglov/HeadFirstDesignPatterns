﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
    public enum PortionSize
    {
        Small,
        Medium,
        Big
    }

    public abstract class Beverage
    {
        protected PortionSize size;

        public void SetSize(PortionSize size)
        {
            this.size = size;
        }

        protected string description = "Unknown Beverage";

        public abstract string GetDescription();

        public abstract double Cost();
    }
}
