﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
    public class Soy : CondimentDecorator
    {
        protected Beverage beverage;

        public Soy(Beverage beverage)
        {
            this.beverage = beverage;
        }

        public override double Cost()
        {
            double cost = beverage.Cost();
            switch (size)
            {
                case PortionSize.Small:
                    cost += .10;
                    break;
                case PortionSize.Medium:
                    cost += .15;
                    break;
                case PortionSize.Big:
                    cost += .20;
                    break;
            }

            return cost;
        }

        public override string GetDescription()
        {
            return beverage.GetDescription() + ", Soy";
        }
    }
}
