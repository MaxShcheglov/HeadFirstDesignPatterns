﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
    public class HouseBlend : Beverage
    {
        public HouseBlend()
        {
            description = "Hose Blend Coffee";
        }

        public override double Cost()
        {
            return .89;
        }

        public override string GetDescription()
        {
            return description;
        }
    }
}
