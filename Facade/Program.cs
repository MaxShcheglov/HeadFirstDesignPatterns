﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    class Program
    {
        static void Main(string[] args)
        {
            HomeTheaterFacade homeTheater = new HomeTheaterFacade(
                new Amplifier(),
                new Tuner(),
                new DvdPlayer(),
                new CdPlayer(),
                new Projector(),
                new Screen(),
                new TheaterLights(),
                new PopcornPopper());

            homeTheater.WatchMovie("Raiders of the Lost Ark");
            homeTheater.EndMovie();

            Console.ReadKey();
        }
    }

    public class Amplifier
    {
        Tuner tuner;
        DvdPlayer dvdPlayer;
        CdPlayer cdPlayer;

        public void On() { }
        public void Off() { }
        public void SetCd() { }
        public void SetDvd() { }
        public void SetStereoSound() { }
        public void SetSurroundSound() { }
        public void SetTuner() { }
        public void SetVolume() { }
        public override string ToString()
        {
            return base.ToString();
        }

    }
    public class DvdPlayer
    {
        Amplifier amplifier;

        public void On() { }
        public void Off() { }
        public void Eject() { }
        public void Pause() { }
        public void Play() { }
        public void SetSurroundAudio() { }
        public void SetTwoChannelAudio() { }
        public void Stop() { }
    }

    public class CdPlayer
    {
        Amplifier amplifier;

        public void On() { }
        public void Off() { }
        public void Eject() { }
        public void Pause() { }
        public void Play() { }
        public void Stop() { }
        public override string ToString()
        {
            return base.ToString();
        }
    }

    public class Tuner
    {
        public void On() { }
        public void Off() { }
        public void SetAm() { }
        public void SetFm() { }
        public void SetFrequency() { }
        public override string ToString()
        {
            return base.ToString();
        }
    }

    public class Screen
    {
        public void Up() { }
        public void Down() { }
        public override string ToString()
        {
            return base.ToString();
        }
    }

    public class Projector
    {
        DvdPlayer dvdPlayer;

        public void On() { }
        public void Off() { }
        public void TvMode() { }
        public void WideScreenMode() { }
        public override string ToString()
        {
            return base.ToString();
        }
    }

    public class PopcornPopper
    {
        public void On() { }
        public void Off() { }
        public void Pop() { }
        public override string ToString()
        {
            return base.ToString();
        }
    }

   public class TheaterLights
   {
        public void On() { }
        public void Off() { }
        public void Dim() { }
        public override string ToString()
        {
            return base.ToString();
        }
   }

    public class HomeTheaterFacade
    {
        Amplifier amplifier;
        Tuner tuner;
        DvdPlayer dvdPlayer;
        CdPlayer cdPlayer;
        Projector projector;
        TheaterLights lights;
        Screen screen;
        PopcornPopper popper;

        public HomeTheaterFacade(Amplifier amp,
                                 Tuner tuner,
                                 DvdPlayer dvdPlayer,
                                 CdPlayer cdPlayer,
                                 Projector projector,
                                 Screen screen,
                                 TheaterLights lights, 
                                 PopcornPopper popper)
        {
            this.amplifier = amp;
            this.tuner = tuner;
            this.dvdPlayer = dvdPlayer;
            this.cdPlayer = cdPlayer;
            this.projector = projector;
            this.lights = lights;
            this.screen = screen;
            this.popper = popper;
        }

        public void WatchMovie(String movie)
        {
            Console.WriteLine("Get ready to watch a movie...");
            popper.On();
            popper.Pop();
            lights.Dim();
            screen.Down();
            projector.On();
            projector.WideScreenMode();
            amplifier.On();
            amplifier.SetDvd();
            amplifier.SetSurroundSound();
            amplifier.SetVolume();
            dvdPlayer.On();
            dvdPlayer.Play();
        }

        public void EndMovie()
        {
            Console.WriteLine("Shutting movie theater down...");
            popper.Off();
            lights.On();
            screen.Up();
            projector.Off();
            amplifier.Off();
            dvdPlayer.Stop();
            dvdPlayer.Eject();
            dvdPlayer.Off();
        }
    }
}
