﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public interface IEnumerator
    {
        bool HasMoreElements();
        IEnumerable<IEnumerator> NextElement();
    }

    public interface IIterator
    {
        bool HasNext();
        void Remove();
        IEnumerable<IEnumerator> Next();
    }

    public class EnumerationIterator : IIterator
    {
        private IEnumerator enumerator;

        public EnumerationIterator(IEnumerator enumerator)
        {
            this.enumerator = enumerator;
        }

        public bool HasNext()
        {
            return enumerator.HasMoreElements();
        }

        public IEnumerable<IEnumerator> Next()
        {
            return enumerator.NextElement();
        }

        public void Remove()
        {
            throw new NotSupportedException();
        }
    }

    public class IteratorEnumerator : IEnumerator
    {
        IIterator iterator;

        public IteratorEnumerator(IIterator iterator)
        {
            this.iterator = iterator;
        }

        public bool HasMoreElements()
        {
            return iterator.HasNext();
        }

        public IEnumerable<IEnumerator> NextElement()
        {
            return iterator.Next();
        }
    }
}
