﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Observer
{
    public class WeatherData : IObservable<WeatherData>
    {
        private float temperature;
        private float humidity;
        private float pressure;

        private List<IObserver<WeatherData>> observers;

        public WeatherData()
        {
            observers = new List<IObserver<WeatherData>>();
        }

        public void SetMeasurements(float temperature, float humidity, float pressure)
        {
            this.temperature = temperature;
            this.humidity = humidity;
            this.pressure = pressure;
            MeasurementsChanged();
        }

        public void MeasurementsChanged()
        {
            NotifyObservers();
        }

        public void NotifyObservers()
        {
            foreach (var observer in observers)
                observer.OnNext(this);
        }

        public float GetTemperature()
        {
            return temperature;
        }

        public float GetHumidity()
        {
            return humidity;
        }

        public float GetPressure()
        {
            return pressure;
        }

        public IDisposable Subscribe(IObserver<WeatherData> observer)
        {
            if (!observers.Contains(observer))
                observers.Add(observer);

            return new Unsubscriber<WeatherData>(observers, observer);
        }
    }

    internal class Unsubscriber<WeatherData> : IDisposable
    {
        private List<IObserver<WeatherData>> observers;
        private IObserver<WeatherData> observer;

        internal Unsubscriber(List<IObserver<WeatherData>> observers, IObserver<WeatherData> observer)
        {
            this.observers = observers;
            this.observer = observer;
        }

        public void Dispose()
        {
            if (observers.Contains(observer))
                observers.Remove(observer);
        }
    }
}
