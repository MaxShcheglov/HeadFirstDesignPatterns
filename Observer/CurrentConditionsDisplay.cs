﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer
{
    public class CurrentConditionsDisplay : IObserver<WeatherData>, IDisplayElement
    {
        private float temperature;
        private float humidity;
        private IDisposable cancellation;

        public CurrentConditionsDisplay(IObservable<WeatherData> weatherData)
        {
            cancellation = weatherData.Subscribe(this);
        }

        public void Update(float temperature, float humidity, float pressure)
        {
            this.temperature = temperature;
            this.humidity = humidity;
            Display();
        }

        public void Display()
        {
            Console.WriteLine($"Current conditions: {temperature}F degrees and {humidity}% humidity");
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnNext(WeatherData value)
        {
            Update(value.GetTemperature(), value.GetHumidity(), value.GetPressure());
        }
    }
}
