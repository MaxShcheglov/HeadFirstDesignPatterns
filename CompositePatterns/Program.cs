﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace CompositePatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            DuckSimulator simulator = new DuckSimulator();
            AbstractDuckFactory duckFactory = new CountingDuckFactory();

            simulator.Simulate(duckFactory);

            Console.ReadKey();
        }

        public static string GetCurrentMethodFullName(MethodBase method)
        {
            return string.Format("{0}.{1}",
                    method.ReflectedType.Name,
                    method.Name);
        }
    }

    public class DuckSimulator
    {
        public void Simulate(AbstractDuckFactory duckFactory)
        {
            IQuackable redheadDuck = duckFactory.CreateRedheadDuck();
            IQuackable duckCall = duckFactory.CreateDuckCall();
            IQuackable rubberDuck = duckFactory.CreateRubberDuck();
            IQuackable gooseDuck = new GooseAdapter(new Goose());

            Console.WriteLine("Duck Simulator: With Composite - Flocks\n");

            Flock flockOfDucks = new Flock();

            flockOfDucks.Add(redheadDuck);
            flockOfDucks.Add(duckCall);
            flockOfDucks.Add(rubberDuck);
            flockOfDucks.Add(gooseDuck);

            Flock flockOfMallards = new Flock();

            IQuackable mallardOne = duckFactory.CreateMallardDuck();
            IQuackable mallardTwo = duckFactory.CreateMallardDuck();
            IQuackable mallardThree = duckFactory.CreateMallardDuck();
            IQuackable mallardFour = duckFactory.CreateMallardDuck();

            flockOfMallards.Add(mallardOne);
            flockOfMallards.Add(mallardTwo);
            flockOfMallards.Add(mallardThree);
            flockOfMallards.Add(mallardFour);

            flockOfDucks.Add(flockOfMallards);

            //Console.WriteLine("\nDuck Simulator: Whole Flock Simulation");
            //Simulate(flockOfDucks);

            //Console.WriteLine("\nDuck Simulator: Mallard Flock Simulation");
            //Simulate(flockOfMallards);

            Quackologist quackologist = new Quackologist();
            flockOfDucks.RegisterObserver(quackologist);

            Simulate(flockOfDucks);

            Console.WriteLine($"\nThe ducks quacked {QuackCounter.GetQuacks()} times");
        }

        private void Simulate(IQuackable duck) => duck.Quack();
    }

    public interface IQuackable : IQuackObservable
    {
        void Quack();
    }

    public class MallardDuck : IQuackable
    {
        private Observable observable;

        public MallardDuck()
        {
            observable = new Observable(this);
        }

        public void NotifyObservers()
        {
            observable.NotifyObservers();
        }

        public void Quack()
        {
            Console.WriteLine(Program.GetCurrentMethodFullName(MethodBase.GetCurrentMethod()));
            NotifyObservers();
        }

        public void RegisterObserver(IObserver observer)
        {
            observable.RegisterObserver(observer);
        }
    }

    public class RedheadDuck : IQuackable
    {
        private Observable observable;

        public RedheadDuck()
        {
            observable = new Observable(this);
        }

        public void NotifyObservers()
        {
            observable.NotifyObservers();
        }

        public void Quack()
        {
            Console.WriteLine(Program.GetCurrentMethodFullName(MethodBase.GetCurrentMethod()));
            NotifyObservers();
        }

        public void RegisterObserver(IObserver observer)
        {
            observable.RegisterObserver(observer);
        }
    }

    public class DuckCall : IQuackable
    {
        private Observable observable;

        public DuckCall()
        {
            observable = new Observable(this);
        }

        public void NotifyObservers()
        {
            observable.NotifyObservers();
        }

        public void Quack()
        {
            Console.WriteLine(Program.GetCurrentMethodFullName(MethodBase.GetCurrentMethod()));
            NotifyObservers();
        }

        public void RegisterObserver(IObserver observer)
        {
            observable.RegisterObserver(observer);
        }
    }

    public class RubberDuck : IQuackable
    {
        private Observable observable;

        public RubberDuck()
        {
            observable = new Observable(this);
        }

        public void NotifyObservers()
        {
            observable.NotifyObservers();
        }

        public void Quack()
        {
            Console.WriteLine(Program.GetCurrentMethodFullName(MethodBase.GetCurrentMethod()));
            NotifyObservers();
        }

        public void RegisterObserver(IObserver observer)
        {
            observable.RegisterObserver(observer);
        }
    }

    public class Goose : IQuackable
    {
        private Observable observable;

        public Goose()
        {
            observable = new Observable(this);
        }

        public void Honk()
        {
            Console.WriteLine(Program.GetCurrentMethodFullName(MethodBase.GetCurrentMethod()));
            NotifyObservers();
        }

        public void NotifyObservers()
        {
            observable.NotifyObservers();
        }

        public void Quack()
        {
            throw new NotImplementedException();
        }

        public void RegisterObserver(IObserver observer)
        {
            observable.RegisterObserver(observer);
        }
    }

    public class GooseAdapter : IQuackable
    {
        private Goose goose;

        public GooseAdapter(Goose goose)
        {
            this.goose = goose;
        }

        public void NotifyObservers()
        {
            goose.NotifyObservers();
        }

        public void Quack()
        {
            goose.Honk();
        }

        public void RegisterObserver(IObserver observer)
        {
            goose.RegisterObserver(observer);
        }
    }

    public class QuackCounter : IQuackable
    {
        private IQuackable duck;
        static int numberOfQuacks;

        public QuackCounter(IQuackable duck)
        {
            this.duck = duck;
        }

        public void Quack()
        {
            duck.Quack();
            numberOfQuacks++;
        }

        public static int GetQuacks()
        {
            return numberOfQuacks;
        }

        public void RegisterObserver(IObserver observer)
        {
            duck.RegisterObserver(observer);
        }

        public void NotifyObservers()
        {
            duck.NotifyObservers();
        }
    }
    
    public abstract class AbstractDuckFactory
    {
        public abstract IQuackable CreateMallardDuck();
        public abstract IQuackable CreateRedheadDuck();
        public abstract IQuackable CreateDuckCall();
        public abstract IQuackable CreateRubberDuck();
    }

    public class DuckFactory : AbstractDuckFactory
    {
        public override IQuackable CreateDuckCall()
        {
            return new DuckCall();
        }

        public override IQuackable CreateMallardDuck()
        {
            return new MallardDuck();
        }

        public override IQuackable CreateRedheadDuck()
        {
            return new RedheadDuck();
        }

        public override IQuackable CreateRubberDuck()
        {
            return new RubberDuck();
        }
    }

    public class CountingDuckFactory : AbstractDuckFactory
    {
        public override IQuackable CreateDuckCall()
        {
            return new QuackCounter(new DuckCall());
        }

        public override IQuackable CreateMallardDuck()
        {
            return new QuackCounter(new MallardDuck());
        }

        public override IQuackable CreateRedheadDuck()
        {
            return new QuackCounter(new RedheadDuck());
        }

        public override IQuackable CreateRubberDuck()
        {
            return new QuackCounter(new RubberDuck());
        }
    }

    public class Flock : IQuackable
    {
        private List<IQuackable> quackers = new List<IQuackable>();

        public void Add(IQuackable quacker)
        {
            quackers.Add(quacker);
        }

        public void NotifyObservers()
        {
            foreach (IQuackable quacker in quackers)
                quacker.NotifyObservers();
        }

        public void Quack()
        {
            IEnumerator iterator = quackers.GetEnumerator();
            while (iterator.MoveNext())
                ((IQuackable)iterator.Current).Quack();
        }

        public void RegisterObserver(IObserver observer)
        {
            foreach (IQuackable quacker in quackers)
                quacker.RegisterObserver(observer);
        }
    }

    public interface IObserver
    {
        void Update(IQuackObservable duck);
    }

    public interface IQuackObservable
    {
        void RegisterObserver(IObserver observer);
        void NotifyObservers();
    }

    public class Observable : IQuackObservable
    {
        private List<object> observers = new List<object>();
        private IQuackObservable duck;

        public Observable(IQuackObservable duck)
        {
            this.duck = duck;
        }

        public void NotifyObservers()
        {
            IEnumerator iterator = observers.GetEnumerator();
            while (iterator.MoveNext())
                ((IObserver)iterator.Current).Update(duck);
        }

        public void RegisterObserver(IObserver observer)
        {
            observers.Add(observer);
        }
    }

    public class Quackologist : IObserver
    {
        public void Update(IQuackObservable duck)
        {
            Console.WriteLine($"Quackologist: {duck} just quacked.");
        }
    }
}
