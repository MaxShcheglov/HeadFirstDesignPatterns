﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using ModelViewController.Model;
using ModelViewController.View;
using ModelViewController.Controller;

namespace ModelViewController
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new DJView());
        }
    }
}
