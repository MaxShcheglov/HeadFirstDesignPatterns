﻿using System.Collections.Generic;

namespace ModelViewController.Model
{
    public class HeartModel : IHeartModel
    {
        List<IBeatObserver> beatObservers = new List<IBeatObserver>();
        List<IBPMObserver> bpmObservers = new List<IBPMObserver>();
        int rate = 68;



        public int GetHeartRate()
        {
            return rate;
        }

        public void RegisterObserver(IBeatObserver o)
        {
            beatObservers.Add(o);
        }

        public void RegisterObserver(IBPMObserver o)
        {
            bpmObservers.Add(o);
        }

        public void RemoveObserver(IBeatObserver o)
        {
            beatObservers.Remove(o);
        }

        public void RemoveObserver(IBPMObserver o)
        {
            bpmObservers.Remove(o);
        }
    }
}