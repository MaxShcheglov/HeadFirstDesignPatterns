﻿namespace ModelViewController.View
{
    partial class DJView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.beatProgressBar = new System.Windows.Forms.ProgressBar();
            this.bpmOutputLabel = new System.Windows.Forms.Label();
            this.btnIncreaseBPM = new System.Windows.Forms.Button();
            this.btnDecreaseBPM = new System.Windows.Forms.Button();
            this.btnSetBPM = new System.Windows.Forms.Button();
            this.tbBPM = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dJControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // beatProgressBar
            // 
            this.beatProgressBar.Location = new System.Drawing.Point(12, 226);
            this.beatProgressBar.Name = "beatProgressBar";
            this.beatProgressBar.Size = new System.Drawing.Size(260, 23);
            this.beatProgressBar.TabIndex = 0;
            // 
            // bpmOutputLabel
            // 
            this.bpmOutputLabel.Location = new System.Drawing.Point(9, 199);
            this.bpmOutputLabel.Name = "bpmOutputLabel";
            this.bpmOutputLabel.Size = new System.Drawing.Size(100, 24);
            this.bpmOutputLabel.TabIndex = 1;
            // 
            // btnIncreaseBPM
            // 
            this.btnIncreaseBPM.Location = new System.Drawing.Point(197, 90);
            this.btnIncreaseBPM.Name = "btnIncreaseBPM";
            this.btnIncreaseBPM.Size = new System.Drawing.Size(75, 23);
            this.btnIncreaseBPM.TabIndex = 11;
            this.btnIncreaseBPM.Text = "Increase";
            this.btnIncreaseBPM.UseVisualStyleBackColor = true;
            this.btnIncreaseBPM.Click += new System.EventHandler(this.btnIncreaseBPM_Click);
            // 
            // btnDecreaseBPM
            // 
            this.btnDecreaseBPM.Location = new System.Drawing.Point(115, 90);
            this.btnDecreaseBPM.Name = "btnDecreaseBPM";
            this.btnDecreaseBPM.Size = new System.Drawing.Size(75, 23);
            this.btnDecreaseBPM.TabIndex = 10;
            this.btnDecreaseBPM.Text = "Decrease";
            this.btnDecreaseBPM.UseVisualStyleBackColor = true;
            this.btnDecreaseBPM.Click += new System.EventHandler(this.btnDecreaseBPM_Click);
            // 
            // btnSetBPM
            // 
            this.btnSetBPM.Location = new System.Drawing.Point(115, 61);
            this.btnSetBPM.Name = "btnSetBPM";
            this.btnSetBPM.Size = new System.Drawing.Size(157, 23);
            this.btnSetBPM.TabIndex = 9;
            this.btnSetBPM.Text = "Set BPM";
            this.btnSetBPM.UseVisualStyleBackColor = true;
            this.btnSetBPM.Click += new System.EventHandler(this.btnSetBPM_Click);
            // 
            // tbBPM
            // 
            this.tbBPM.Location = new System.Drawing.Point(179, 35);
            this.tbBPM.Name = "tbBPM";
            this.tbBPM.Size = new System.Drawing.Size(93, 20);
            this.tbBPM.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(112, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Enter BPM:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dJControlToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dJControlToolStripMenuItem
            // 
            this.dJControlToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startToolStripMenuItem,
            this.stopToolStripMenuItem,
            this.quitToolStripMenuItem});
            this.dJControlToolStripMenuItem.Name = "dJControlToolStripMenuItem";
            this.dJControlToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.dJControlToolStripMenuItem.Text = "DJ Control";
            // 
            // startToolStripMenuItem
            // 
            this.startToolStripMenuItem.Name = "startToolStripMenuItem";
            this.startToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.startToolStripMenuItem.Text = "Start";
            this.startToolStripMenuItem.Click += new System.EventHandler(this.startToolStripMenuItem_Click);
            // 
            // stopToolStripMenuItem
            // 
            this.stopToolStripMenuItem.Name = "stopToolStripMenuItem";
            this.stopToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.stopToolStripMenuItem.Text = "Stop";
            this.stopToolStripMenuItem.Click += new System.EventHandler(this.stopToolStripMenuItem_Click);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.quitToolStripMenuItem.Text = "Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
            // 
            // DJView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnIncreaseBPM);
            this.Controls.Add(this.btnDecreaseBPM);
            this.Controls.Add(this.btnSetBPM);
            this.Controls.Add(this.tbBPM);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.bpmOutputLabel);
            this.Controls.Add(this.beatProgressBar);
            this.Name = "DJView";
            this.Text = "MainView";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar beatProgressBar;
        private System.Windows.Forms.Label bpmOutputLabel;
        private System.Windows.Forms.Button btnIncreaseBPM;
        private System.Windows.Forms.Button btnDecreaseBPM;
        private System.Windows.Forms.Button btnSetBPM;
        private System.Windows.Forms.TextBox tbBPM;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dJControlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
    }
}

