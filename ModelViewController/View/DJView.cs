﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ModelViewController.Model;
using ModelViewController.Controller;

namespace ModelViewController.View
{
    public partial class DJView : Form, IBeatObserver, IBPMObserver
    {
        IBeatModel model;
        IController controller;

        public DJView()
        {
            InitializeComponent();

            //model = new BeatModel();
            //controller = new BeatController(model, this);

            IHeartModel heartModel = new HeartModel();
            
            model = new HeartAdapter(heartModel);
            controller = new HeartController(heartModel, this);

            model.RegisterObserver((IBeatObserver)this);
            model.RegisterObserver((IBPMObserver)this);

            beatProgressBar.Step = 1;
            beatProgressBar.Maximum = 100;
        }

        public void UpdateBeat()
        {
            beatProgressBar.Value = model.GetBPM(); ;
        }

        public void UpdateBPM()
        {
            bpmOutputLabel.Text = model.GetBPM() == 0 ? "Offline" : $"Current BPM: {model.GetBPM()}";
        }

        public void EnableStopMenuItem()
        {
            stopToolStripMenuItem.Enabled = true;
        }

        public void DisableStopMenuItem()
        {
            stopToolStripMenuItem.Enabled = false;
        }

        public void EnableStartMenuItem()
        {
            startToolStripMenuItem.Enabled = true;
        }

        public void DisableStartMenuItem()
        {
            startToolStripMenuItem.Enabled = false;
        }

        private void btnSetBPM_Click(object sender, EventArgs e)
        {
            controller.SetBPM(int.Parse(tbBPM.Text));
        }

        private void btnDecreaseBPM_Click(object sender, EventArgs e)
        {
            controller.DecreaseBPM();
        }

        private void btnIncreaseBPM_Click(object sender, EventArgs e)
        {
            controller.IncreaseBPM();
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controller.Start();
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controller.Stop();
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
