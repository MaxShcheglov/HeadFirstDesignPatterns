﻿namespace ModelViewController.Controller
{
    public interface IController
    {
        void DecreaseBPM();
        void IncreaseBPM();
        void SetBPM(int bpm);
        void Start();
        void Stop();
    }
}