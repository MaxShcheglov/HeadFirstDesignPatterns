﻿using ModelViewController.Model;
using ModelViewController.View;

namespace ModelViewController.Controller
{
    public class BeatController : IController
    {
        IBeatModel model;
        DJView view;

        public BeatController(IBeatModel model, DJView view)
        {
            this.model = model;
            //view = new DJView(this, model);
            this.view = view;
            view.DisableStopMenuItem();
            view.EnableStartMenuItem();
            model.Initialize();
        }

        public void SetBPM(int bpm)
        {
            model.SetBPM(bpm);
        }

        public void IncreaseBPM()
        {
            model.SetBPM(model.GetBPM() + 1);
        }

        public void DecreaseBPM()
        {
            model.SetBPM(model.GetBPM() - 1);
        }

        public void Start()
        {
            model.On();
            view.DisableStartMenuItem();
            view.EnableStopMenuItem();
        }

        public void Stop()
        {
            model.Off();
            view.EnableStartMenuItem();
            view.DisableStopMenuItem();
        }
    }
}
