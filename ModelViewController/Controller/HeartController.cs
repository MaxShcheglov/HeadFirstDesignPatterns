﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ModelViewController.Model;
using ModelViewController.View;

namespace ModelViewController.Controller
{
    public class HeartController : IController
    {
        IHeartModel model;
        DJView view;

        public HeartController(IHeartModel model, DJView view)
        {
            this.model = model;
            this.view = view;
            view.DisableStopMenuItem();
            view.DisableStartMenuItem();
        }

        public void DecreaseBPM() { }

        public void IncreaseBPM() { }

        public void SetBPM(int bpm) { }

        public void Start() { }

        public void Stop() { }
    }
}
