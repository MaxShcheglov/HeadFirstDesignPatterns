﻿namespace ModelViewController.Common
{
    public interface ISequencer
    {
        void Start();
        void Stop();
        void SetTempoInBPM(int bpm);
        void NotifyBPMObservers();
        void NotifyBeatObservers();
    }
}
