﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelViewController.Model
{
    public class HeartAdapter : IBeatModel
    {
        private IHeartModel heart;

        public HeartAdapter(IHeartModel heart)
        {
            this.heart = heart;
        }

        public int GetBPM()
        {
            return heart.GetHeartRate();
        }

        public void Initialize() { }

        public void Off() { }

        public void On() { }

        public void RegisterObserver(IBeatObserver o)
        {
            heart.RegisterObserver(o);
        }

        public void RegisterObserver(IBPMObserver o)
        {
            heart.RegisterObserver(o);
        }

        public void RemoveObserver(IBeatObserver o)
        {
            heart.RemoveObserver(o);
        }

        public void RemoveObserver(IBPMObserver o)
        {
            heart.RemoveObserver(o);
        }

        public void SetBPM(int bpm) { }

    }
}
