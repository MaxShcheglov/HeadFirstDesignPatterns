﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelViewController.Model
{
    public interface IHeartModel
    {
        int GetHeartRate();
        void RegisterObserver(IBeatObserver o);
        void RegisterObserver(IBPMObserver o);
        void RemoveObserver(IBeatObserver o);
        void RemoveObserver(IBPMObserver o);
    }
}
