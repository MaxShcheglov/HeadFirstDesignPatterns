﻿namespace ModelViewController.Model
{
    public interface IBPMObserver
    {
        void UpdateBPM();
    }
}
