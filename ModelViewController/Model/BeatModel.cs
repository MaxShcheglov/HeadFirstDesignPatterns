﻿using System;
using System.Collections.Generic;

using ModelViewController.Common;

namespace ModelViewController.Model
{
    public class BeatModel : IBeatModel
    {
        ISequencer sequencer;
        List<IBeatObserver> beatObservers = new List<IBeatObserver>();
        List<IBPMObserver> bpmObservers = new List<IBPMObserver>();
        int bpm = 90;

        public void Initialize()
        {
            SetUpMidi();
            BuildTrackAndStart();
        }

        private void SetUpMidi() { }
        private void BuildTrackAndStart() { }

        public void Off()
        {
            SetBPM(0);
            //sequencer.Stop();
        }

        public void On()
        {
            //sequencer.Start();
            SetBPM(90);
        }

        public void RegisterObserver(IBeatObserver o)
        {
            beatObservers.Add(o);
        }

        public void RegisterObserver(IBPMObserver o)
        {
            bpmObservers.Add(o);
        }

        public void RemoveObserver(IBeatObserver o)
        {
            beatObservers.Remove(o);
        }

        public void RemoveObserver(IBPMObserver o)
        {
            bpmObservers.Remove(o);
        }

        public void SetBPM(int bpm)
        {
            this.bpm = bpm;
            //sequencer.SetTempoInBPM(GetBPM());
            NotifyBPMObservers();
            NotifyBeatObservers();
        }

        private void NotifyBPMObservers()
        {
            //bpmObservers.ForEach(observer => observer.UpdateBPM());
            foreach (IBPMObserver bpmObserver in bpmObservers)
                bpmObserver.UpdateBPM();
        }

        public int GetBPM()
        {
            return bpm;
        }

        private void BeatEvent()
        {
            NotifyBeatObservers();
        }

        private void NotifyBeatObservers()
        {
            //beatObservers.ForEach(observer => observer.UpdateBeat());
            foreach (IBeatObserver beatObserver in beatObservers)
                beatObserver.UpdateBeat();
        }
    }
}
