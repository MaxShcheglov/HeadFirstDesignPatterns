﻿namespace ModelViewController.Model
{
    public interface IBeatObserver
    {
        void UpdateBeat();
    }
}
