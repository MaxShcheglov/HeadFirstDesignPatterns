﻿namespace ModelViewController.Model
{
    public interface IBeatModel
    {
        int GetBPM();
        void Initialize();
        void Off();
        void On();
        void RegisterObserver(IBeatObserver o);
        void RegisterObserver(IBPMObserver o);
        void RemoveObserver(IBeatObserver o);
        void RemoveObserver(IBPMObserver o);
        void SetBPM(int bpm);
    }
}