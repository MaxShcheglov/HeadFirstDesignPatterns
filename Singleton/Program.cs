﻿using System;

namespace Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    class ClassicSingleton
    {
        private static ClassicSingleton uniqueInstance;

        private ClassicSingleton() { }

        public static ClassicSingleton GetInstance()
        {
            if (uniqueInstance == null)
                uniqueInstance = new ClassicSingleton();

            return uniqueInstance;
        }
    }

    class MultithreadSingleton
    {
        private volatile static MultithreadSingleton uniqueInstance = new MultithreadSingleton();
        private static object lockObject = new Object();

        private MultithreadSingleton() { }

        public static MultithreadSingleton GetInstance()
        {
            if (uniqueInstance == null)
                lock(lockObject)
                {
                    if (uniqueInstance == null)
                        uniqueInstance = new MultithreadSingleton();
                }            

            return uniqueInstance;
        }
    }
}
