﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateMethod
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public abstract class CaffeineBeverage
    {
        public void PrepareRecipe()
        {
            BoilWater();
            Brew();
            PourInCup();

            if (CustomerWantsCondiments())
                AddCondiments();
        }

        public void BoilWater() { }

        public void PourInCup() { }

        public abstract void Brew();

        public abstract void AddCondiments();

        public virtual bool CustomerWantsCondiments()
        {
            return true;
        }
    }

    public class Coffee : CaffeineBeverage
    {
        public void BrewCoffeeGrinds() { }

        public void AddSugarAndMilk() { }

        public override void Brew()
        {
            BrewCoffeeGrinds();
        }

        public override void AddCondiments()
        {
            AddSugarAndMilk();
        }

        public override bool CustomerWantsCondiments()
        {
            return base.CustomerWantsCondiments();
        }
    }

    public class Tea : CaffeineBeverage
    {
        public void SteepTeaBag() { }

        public void AddLemon() { }

        public override void Brew()
        {
            SteepTeaBag();
        }

        public override void AddCondiments()
        {
            AddLemon();
        }

        public override bool CustomerWantsCondiments()
        {
            return base.CustomerWantsCondiments();
        }
    }
}
