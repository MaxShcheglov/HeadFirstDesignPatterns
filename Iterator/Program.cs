﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Iterator
{
    class Program
    {
        static void Main(string[] args)
        {
            PancakeHouseMenu pancakeHouseMenu = new PancakeHouseMenu();
            DinerMenu dinerMenu = new DinerMenu();
            CafeMenu cafeMenu = new CafeMenu();

            Waitress waitress = new Waitress( new ArrayList() { pancakeHouseMenu, dinerMenu, cafeMenu });

            waitress.PrintMenu();

            Console.ReadKey();
        }
    }

    public interface IIterator
    {
        bool HasNext();
        object Next();
    }

    public class DinerMenuIterator : IEnumerator
    {
        private MenuItem[] items;
        private int currentIndex;
        private MenuItem currentItem;

        public DinerMenuIterator(MenuItem[] items)
        {
            this.items = items;
            currentIndex = -1;
            currentItem = default(MenuItem);
        }

        public object Current => currentItem;

        public void Dispose() { }

        public bool MoveNext()
        {
            currentIndex++;
            if (currentIndex >= items.Length || items[currentIndex] == null)
            {
                return false;
            }
            else
            {
                currentItem = items[currentIndex];
            }
            return true;
        }

        public void Reset()
        {
            currentIndex = -1;
        }
    }

    public class AlternatingDinerMenuIterator : IEnumerator
    {
        private MenuItem[] items;
        private int currentPosition;
        private MenuItem currentItem;

        public AlternatingDinerMenuIterator(MenuItem[] items)
        {
            this.items = items;
            currentPosition = ((int)DateTime.Now.DayOfWeek) % 2;
            currentItem = default(MenuItem);
        }

        public object Current
        {
            get
            {
                MenuItem menuItem = items[currentPosition];
                currentPosition += 2;
                return menuItem;
            }
        }

        public bool MoveNext()
        {
            return (currentPosition >= items.Length || items[currentPosition] == null) ? false : true;
        }

        public void Reset()
        {
            currentPosition = -1;
        }
    }

    public class PancakeHouseIterator : IEnumerator
    {
        private ArrayList items;
        private int currentPosition = 0;
        private MenuItem currentItem;

        public PancakeHouseIterator(ArrayList items)
        {
            this.items = items;
            currentPosition = -1;
            currentItem = default(MenuItem);
        }

        public MenuItem Current => currentItem;

        object IEnumerator.Current => currentItem;

        public void Dispose() { }

        public bool MoveNext()
        {
            currentPosition++;
            if (currentPosition >= items.Count || items[currentPosition] == null)
            {
                return false;
            }
            else
            {
                currentItem = (MenuItem)items[currentPosition];
            }
            return true;
        }

        public void Reset()
        {
            currentPosition = -1;
        }
    }

    public class MenuItem
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public bool Vegeratarian { get; private set; }
        public double Price { get; private set; }

        public MenuItem(string name,
                        string description,
                        bool vegetarian,
                        double price)
        {
            Name = name;
            Description = description;
            Vegeratarian = vegetarian;
            Price = price;
        }
    }

    public class PancakeHouseMenu : IMenu
    {
        private ArrayList menuItems;

        public PancakeHouseMenu()
        {
            menuItems = new ArrayList();

            AddItem("K&B's pancakes breakfast", "with scrambled eggs and toast", true, 2.99);
        }

        public void AddItem(string name,
                            string description,
                            bool vegetarian,
                            double price)
        {
            MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
            menuItems.Add(menuItem);
        }

        public IEnumerator CreateIterator()
        {
            return menuItems.GetEnumerator();
        }
    }

    public class DinerMenu : IMenu
    {
        private const int maxItems = 1;
        private int numberOfItems = 0;
        private MenuItem[] menuItems;

        public DinerMenu()
        {
            menuItems = new MenuItem[maxItems];

            AddItem("Vegetarian", "bacon with lettuce & tomato on whole wheat", true, 2.99);
        }

        public void AddItem(string name, string description, bool vegetarian, double price)
        {
            MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
            if (numberOfItems >= maxItems)
            {
                Console.WriteLine("Sorry, I cant");
            }
            else
            {
                menuItems[numberOfItems] = menuItem;
                numberOfItems++;
            }
        }

        public IEnumerator CreateIterator()
        {
            return menuItems.GetEnumerator();
        }
    }

    public interface IMenu
    {
        IEnumerator CreateIterator();
    }

    public class Waitress
    {
        private ArrayList menus;
        private IMenu pancakeHouseMenu;
        private IMenu dinerMenu;
        private IMenu cafeMenu;

        public Waitress(ArrayList menus)
        {
            this.menus = menus;
        }

        public void PrintMenu()
        {
            IEnumerator menuIterator = menus.GetEnumerator();
            while (menuIterator.MoveNext())
            {
                IMenu menu = (IMenu)menuIterator.Current;
                PrintMenu(menu.CreateIterator());
            }
        }

        private void PrintMenu(IEnumerator iterator)
        {
            while (iterator.MoveNext())
            {
                MenuItem menuItem = (MenuItem)iterator.Current;
                Console.WriteLine($"{menuItem.Name} {menuItem.Price} {menuItem.Description}");
            }
        }

        public void PrintBreakfastMenu()
        {

        }

        public void PrintLaunchMenu()
        {

        }

        public void PrintVegetarianMenu()
        {

        }

        public bool IsVegetarianMenu(string name)
        {
            return true;
        }

    }

    public class CafeMenu : IMenu
    {
        private Hashtable menuItems = new Hashtable();

        public CafeMenu()
        {
            AddItem("test name", "test desc", true, 3.99);
        }

        public void AddItem(string name, string description, bool vegetarian, double price)
        {
            MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
            menuItems.Add(menuItem.Name, menuItem);
        }

        IEnumerator IMenu.CreateIterator()
        {
            return menuItems.Values.GetEnumerator();
        }
    }
}
